Here is the list of my projects
=================================


### Public projects (teaching and outreach)
 - [Publications](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/publications)  [Public] :: Research publication repository
 - [earth-data-science](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/earth-data-science) [Public] :: This repository contains the Earth Data Science course materials.
 - [vrfieldtrip](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/vrfieldtrip) [Public] :: This is a repository for advertising VR Field Trip dev
 


----------------------------

### Research projects (access upon collaboration)

#### Avalanche/Landslide/River: 
 - [Shaltop](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/shaltop) [Private] :: Shallow water model for landslide and avalanche
 - [sflow2d](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/sflow2d)  [Private] :: another 2D Shallow water code
 - [avaSAR](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/avasar) [Private] :: Tools for extracting avalanche roughness from SAR observations
 - [wavalanche](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/wavalanche) [Private] :: Toolbox for avalanche seismology
 - [myRiver](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/myriver) [Private] :: python code that solves ODE for sediment transport in river

#### Remote-sensing:
 - [LiiD](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/liid) [Private] :: Deep-Learning processing for 3D Cloud LidAr
 - [matISIS](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/matisis) [Private] :: Matlab toolbox for the USGS ISIS software
 - [panoMtBlanc](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/panomontblanc) [Private] :: Webcam time series processing of the Mont Blanc valley
 - [MM-ToolBox](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/micmac_toolbox) [Private] :: MiMac Toolbox (sensor model naviguation and quality control)
 - [SAR_Backscatter](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/radar_backscatter) [Private] :: SAR Backscatter modelling 
 - [socetset_toolbox](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/socetset_toolbox) [Private] :: Socet Set Toolbox for planetary photogrammetry
 - [sentinel_toolbox](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/sentinel_toolbox) [Private] :: Toolbox for processing Sentinel-1 data

#### Geochemistry:
 - [pyCrunch](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/pycrunch) [Private] :: Python overlay for a reactive transport (Crunch) code
 

#### Archives/Documentations: 
 - [earth-data-science-labs_2019-2020](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/earth-data-science-labs) [Private] :: 2019-2020 archive of EDS repository
 - [ERC_template](https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/erc_template) [Private] :: ERC proposal LatEx template
 